package com.ushi.mockapi.executors;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.ushi.mockapi.CallExecutor;
import com.ushi.mockapi.HttpResponse;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Headers;
import okhttp3.Request;


public class AssetCallExecutor implements CallExecutor {

    private Context mContext;

    private PathResolver mResolver;

    private AssetCallExecutor(Context context, PathResolver resolver) {
        mContext = context.getApplicationContext();
        mResolver = resolver;
    }

    public static AssetCallExecutor create(Context context) {
        return new AssetCallExecutor(context, null);
    }

    public static AssetCallExecutor create(Context context, String headerName) {
        PathFromHeaderResolver resolver = headerName != null ? PathFromHeaderResolver.by(headerName) : null;
        return new AssetCallExecutor(context, resolver);
    }

    @NonNull
    @Override
    public HttpResponse execute(Request request) {
        InputStream is = loadAssets(request);
        if (is == null) {
            return new HttpResponse.Builder()
                    .error(404)
                    .build();
        }

        return new HttpResponse.Builder()
                .code(200)
                .body("application/json", is)
                .build();
    }

    private InputStream loadAssets(Request request) {
        String filePath = requestToFilePath(request);

        try {
            return mContext.getAssets().open(filePath);

        } catch (FileNotFoundException e) {
            return null;

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private String requestToFilePath(Request request) {
        String filePath = null;

        if (mResolver != null) {
            filePath = mResolver.resolve(request);
        }
        if (filePath == null) {
            // default
            filePath = URI.create(request.url().toString()).getPath().split("\\?")[0] + "." + request.method();

            // TODO dependent android sdk
//            filePath = Uri.parse(request.url().toString()).getPath().split("\\?")[0] + "." + request.method();
        }

        if (filePath.startsWith("/")) {
            filePath = filePath.replaceFirst("/", "");
        }

        return filePath;
    }

    public interface PathResolver {

        /**
         * TODO docs
         */
        @Nullable
        String resolve(Request request);
    }

    // TODO create remove header interceptor
    static class PathFromHeaderResolver implements PathResolver {

        private static Map<String, WeakReference<PathFromHeaderResolver>> caches = new HashMap<>();

        private String name;

        private PathFromHeaderResolver(String name) {
            this.name = name;
        }

        @NonNull
        static PathFromHeaderResolver by(@NonNull String name) {
            WeakReference<PathFromHeaderResolver> weak = caches.get(name);
            PathFromHeaderResolver instance = weak != null ? weak.get() : null;
            if (instance == null) {
                instance = new PathFromHeaderResolver(name);
                caches.put(name, new WeakReference<PathFromHeaderResolver>(instance));
            }

            return instance;
        }

        @Nullable
        @Override
        public String resolve(Request request) {
            Headers headers = request.headers();
            if (headers == null || headers.size() == 0) {
                return null;
            }

            String value = headers.get(this.name);
            return value != null ? value + "." + request.method() : null;
        }
    }
}
