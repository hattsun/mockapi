package com.ushi.mockapi;

import android.support.annotation.NonNull;

import okhttp3.Request;


/**
 * TODO リクエストの状態を保持してはいけない
 */
public interface CallExecutor {

    /**
     * @param request {@link okhttp3.Response}
     * @return build by {@link HttpResponse.Builder}
     */
    @NonNull
    HttpResponse execute(Request request);
}
