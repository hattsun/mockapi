package com.ushi.mockapi;


import java.io.IOException;
import java.io.InputStream;

import okio.Okio;

public class HttpResponse {

    public final int code;

    public final String mimeType;

    public final byte[] body;

    public HttpResponse(int code, String mimeType, byte[] body) {
        this.code = code;
        this.mimeType = mimeType;
        this.body = body;
    }

    public static class Builder {

        int code;

        String mimeType;

        byte[] body;

        public HttpResponse build() {
            return new HttpResponse(code, mimeType, body);
        }

        public Builder code(int code) {
            this.code = code;
            return this;
        }

        public Builder error(int code) {
            this.code = code;
            this.mimeType = "text/html";
            this.body = new byte[0];
            return this;
        }

        public Builder body(String mimeType, byte[] body) {
            this.mimeType = mimeType;
            this.body = body;
            return this;
        }

        public Builder body(String mimeType, InputStream body) {
            try {
                return body(mimeType, Okio.buffer(Okio.source(body)).readByteArray());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
