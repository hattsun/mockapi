package com.ushi.mockapi;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;


public class Mockapi implements Call.Factory {

    private static final String TAG = Mockapi.class.getSimpleName();

    private CallExecutor mCallExecutor;

    private final long sleep;

    public Mockapi(CallExecutor callExecutor, long sleep) {
        this.mCallExecutor = callExecutor;
        this.sleep = sleep;
    }

    @Override
    public okhttp3.Call newCall(@NonNull Request request) {
        return new Call(mCallExecutor, request, sleep);
    }

    static class Call implements okhttp3.Call {

        private final CallExecutor mCallExecutor;

        private final long sleep;

        private final Request request;

        private boolean canceled;

        private boolean executed;

        public Call(CallExecutor callExecutor, Request request, long sleep) {
            mCallExecutor = callExecutor;
            this.request = request;
            this.sleep = sleep;
        }

        @Override
        public Request request() {
            return request;
        }

        @Override
        public Response execute() throws IOException {
            synchronized (this) {
                if (executed) {
                    throw new IllegalStateException("Already executed.");
                }
                executed = true;
            }

            long time = System.currentTimeMillis();
            try {
                return executeInternal();

            } finally {
                long s = sleep - (System.currentTimeMillis() - time);
                if (s > 0) {
                    try {
                        Thread.sleep(s);
                    } catch (InterruptedException ignore) {
                    }
                }
            }
        }

        private Response executeInternal() throws IOException {
            HttpResponse httpResponse = mCallExecutor.execute(request);
            return new Response.Builder()
                    .request(request)
                    .protocol(Protocol.HTTP_1_1)
                    .body(ResponseBody.create(MediaType.parse(httpResponse.mimeType), httpResponse.body))
                    .code(httpResponse.code)
                    .message(String.valueOf(httpResponse.code))
                    .build();
        }

        @Override
        public void enqueue(@NonNull final Callback responseCallback) {
            synchronized (this) {
                if (executed) {
                    throw new IllegalStateException("Already executed.");
                }
                executed = true;
            }

            // TODO add to ThreadPoolExecutor

            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (!isCanceled()) {
                        try {
                            responseCallback.onResponse(Call.this, execute());

                        } catch (IOException e) {
                            responseCallback.onFailure(Call.this, e);
                        }
                    }

                }
            }).run();
        }

        @Override
        public void cancel() {
            canceled = true;
        }

        @Override
        public synchronized boolean isExecuted() {
            return executed;
        }

        @Override
        public boolean isCanceled() {
            return canceled;
        }

        @SuppressWarnings("CloneDoesntCallSuperClone")
        @Override
        public okhttp3.Call clone() {
            return new Call(mCallExecutor, request, sleep);
        }
    }
}