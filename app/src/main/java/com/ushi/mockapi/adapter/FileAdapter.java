package com.ushi.mockapi.adapter;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ushi.mockapi.R;
import com.ushi.mockapi.api.model.File;
import com.ushi.mockapi.databinding.ItemFileBinding;

import java.util.ArrayList;
import java.util.List;


public class FileAdapter extends RecyclerView.Adapter<BindingHolder<ItemFileBinding>> {

    private final List<File> mList = new ArrayList<>();

    @Override
    public BindingHolder<ItemFileBinding> onCreateViewHolder(ViewGroup parent, int viewType) {
        return BindingHolder.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_file, parent, false);
    }

    @Override
    public void onBindViewHolder(BindingHolder<ItemFileBinding> holder, int position) {
        holder.dataBinding.setFile(mList.get(position));
        holder.dataBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void set(List<File> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void add(List<File> list) {
        int preSize = mList.size();

        mList.addAll(list);
        notifyItemRangeInserted(preSize, list.size());
    }
}
