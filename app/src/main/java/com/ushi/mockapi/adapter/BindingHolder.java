package com.ushi.mockapi.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class BindingHolder<T extends ViewDataBinding> extends RecyclerView.ViewHolder {

    public final T dataBinding;

    public BindingHolder(View itemView) {
        super(itemView);
        this.dataBinding = DataBindingUtil.bind(itemView);
    }

    public BindingHolder(T dataBinding) {
        super(dataBinding.getRoot());
        this.dataBinding = dataBinding;
    }

    public static <T extends ViewDataBinding> BindingHolder<T> inflate(LayoutInflater inflater, @LayoutRes int layoutId, ViewGroup parent, boolean attachToParent) {
        T tBinding = DataBindingUtil.inflate(inflater, layoutId, parent, attachToParent);
        return new BindingHolder<>(tBinding);
    }
}
