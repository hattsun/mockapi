package com.ushi.mockapi.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;

import com.ushi.mockapi.Mockapi;
import com.ushi.mockapi.R;
import com.ushi.mockapi.adapter.FileAdapter;
import com.ushi.mockapi.api.service.DriveService;
import com.ushi.mockapi.databinding.ActivityMainBinding;
import com.ushi.mockapi.executors.AssetCallExecutor;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    FileAdapter mAdapter;
    private DriveService mDriveService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        mDriveService = new DriveService(
                new Retrofit.Builder()
                        .baseUrl("https://https://www.googleapis.com/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .callFactory(new Mockapi(AssetCallExecutor.create(this), 1500))
                        .build()
        );

        mAdapter = new FileAdapter();
        binding.recycler.setAdapter(mAdapter);
        binding.recycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        refresh();
    }

    private void refresh() {
        mDriveService.filesList(null)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        fileList -> mAdapter.set(fileList.files),
                        Throwable::printStackTrace
                );
    }
}
