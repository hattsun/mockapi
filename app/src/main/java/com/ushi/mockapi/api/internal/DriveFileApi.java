package com.ushi.mockapi.api.internal;


import android.support.annotation.IntRange;
import android.support.annotation.Nullable;

import com.ushi.mockapi.api.model.File;
import com.ushi.mockapi.api.model.FileList;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * https://developers.google.com/drive/v3/reference/files
 */
public interface DriveFileApi {

    @GET("/drive/v3/files")
    Observable<FileList> list(
            @Query("pageToken") @Nullable String pageToken,
            @Query("pageSize") @Nullable @IntRange(from = 1, to = 1000) Integer pageSize);

    @GET("/drive/v3/files/{id}")
    Observable<File> get(@Path("id") String id);
}
