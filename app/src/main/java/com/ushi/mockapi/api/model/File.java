package com.ushi.mockapi.api.model;


/**
 * /drive/v3/files/:id
 */
public class File {

    public String kind;

    public String id;

    public String name;

    public String mimeType;
}
