package com.ushi.mockapi.api.model;

import java.util.Collections;
import java.util.List;

/**
 * GET /drive/v3/files
 */
public class FileList {

    public String kind;

    public String nextPageToken;

    public boolean incompleteSearch;

    public List<File> files = Collections.emptyList();
}
