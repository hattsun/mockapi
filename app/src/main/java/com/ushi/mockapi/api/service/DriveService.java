package com.ushi.mockapi.api.service;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.ushi.mockapi.api.internal.DriveFileApi;
import com.ushi.mockapi.api.model.File;
import com.ushi.mockapi.api.model.FileList;

import io.reactivex.Single;
import retrofit2.Retrofit;


public class DriveService {

    private final Retrofit mRetrofit;

    public DriveService(@NonNull Retrofit retrofit) {
        mRetrofit = retrofit;
    }

    public Single<FileList> filesList(@Nullable String pageToken) {
        return mRetrofit.create(DriveFileApi.class)
                .list(pageToken, 10)
                .singleOrError();
    }

    public Single<File> filesGet(String id) {
        return mRetrofit.create(DriveFileApi.class).get(id).singleOrError();
    }
}
